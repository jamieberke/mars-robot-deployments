import type { NextPage } from "next";
import Head from "next/head";
import Pleateu from "../components/plateu/plateu";

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Mars Robot App</title>
        <meta
          name="description"
          content="Application to track mars robot movements"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Pleateu />
      </main>
    </div>
  );
};

export default Home;

# Mars Robot Deployments

## Dependencies 

- NodeJS 12.2.2 or later
- NPM

## How it was built

- React
- NextJs
- Axios
- Tailwind CSS
- HTML5 Canvas
- Aria accessibility where applicable

## How it works

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

### Mars

This app represents a robot deployment mission to mars. HTML5 `<canvas>` is used to draw a plateu to represent the land on mars. This can be configured using `CanvasOptions` so you can choose the `canvas` size as well as the grid layout columns/rows. Canvas was chosen due to it being better for performance.Unfortunately using canvas for the robot animations was too complex as it involved many redraws so I opted for `keyframe` animation. I have added `aria` attributes where necessary so that elements like `<canvas>` can be interpreted by screen readers. 

### Robots + Animations

The robots are positioned on mars relative to the `canvas` coordinate system. Each cell represents a positional coordinate rather than a position within the canvas coordinate system. The robots are initially positioned using their positinal coordinates relative to the `canvas` size, number of rows/columns and the cell size. A canvas size of 500 with 10 rows and 10 columns will give a cell size of 50px meaning 1,1 is 50px,50px). The robot is then positioned centrally within the cell by using the cell size, relative robot diameter and a relative amount of padding.

The animations are generated from an array of movements from the data set. These animations are performed as keyframes. The number of keyframes is calculated depending on the number of movements the robot is set to take. Each robot performs their movements sequentially by using `animation-delay` calculated using the `robot.id` and the `animation_duration`. The movement animations are performed using the robots current offset within the `canvas` and the relative `canvas` cell size.

### Data

The app works using a free mocked API from mocky.io. If for whatever reason the API is no longer available then you can create a new one here `https://designer.mocky.io/design`. Once created, use the `constants.js` file to update the `BASE_URL` and `ROBOT_ENDPOINT` to match the mocky API. You can find the example dataset inside `mocks/robots.json`;

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

import useAxios from "axios-hooks";
import { useState } from "react";
import { BASE_URL, ROBOT_ENDPOINT } from "../../constants";
import Robot, { I_Robot } from "../robot/robot";
import { Canvas, CanvasOptions } from "../shared/canvas/canvas";

export const canvas_options = new CanvasOptions();

const Pleateu = () => {
  const [{ data, loading, error }] = useAxios(`${BASE_URL}${ROBOT_ENDPOINT}`);
  const [robots_deployed, setRobotsDeployed] = useState(false);

  return (
    <div className="p-10">
      <h1 className="text-xl font-medium mb-5">Mars Robot Deployments</h1>

      {loading || !data ? (
        <p>loading...</p>
      ) : (
        <div className="flex">
          {error && (
            <div>
              <p>{error}</p>
            </div>
          )}
          <div
            className="relative"
            style={{
              width: canvas_options.width,
              height: canvas_options.height,
            }}
          >
            <Canvas options={canvas_options} />
            {robots_deployed &&
              data.map((robot: I_Robot) => (
                <Robot
                  robot={robot}
                  canvas_options={canvas_options}
                  key={robot.id}
                />
              ))}
          </div>
          <button
            role="button"
            onClick={() => setRobotsDeployed(true)}
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded h-full ml-4"
          >
            Deploy Robots
          </button>
        </div>
      )}
    </div>
  );
};

export default Pleateu;

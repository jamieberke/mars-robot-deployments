import { useRef, useEffect } from "react";
import { CanvasOptions } from "./canvas";

const drawGridLines = (
  context: CanvasRenderingContext2D,
  options: CanvasOptions
) => {
  const { rows, columns, width, height } = options;
  const cellWidth = width / columns;
  const cellHeight = height / columns;
  for (let i = 1; i < rows; i += 1) {
    // Create Y axix grid lines
    context.beginPath();
    context.moveTo(0, cellHeight * i);
    context.lineTo(height, cellHeight * i);
    context.stroke();
  }

  for (let i = 1; i < columns; i += 1) {
    // Create X axix grid lines
    context.beginPath();
    context.moveTo(cellWidth * i, 0);
    context.lineTo(cellWidth * i, width);
    context.stroke();
  }
};

const useCanvas = (options: CanvasOptions) => {
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    if (canvas) {
      const context = canvas.getContext("2d");
      if (options.width) canvas.width = options.width;
      if (options.height) canvas.height = options.height;
      if (context) {
        drawGridLines(context, options);
      }
    }
  }, [options]);

  return canvasRef;
};

export default useCanvas;

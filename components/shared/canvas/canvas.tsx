import React from "react";
import useCanvas from "./hooks";

export class CanvasOptions {
  public width: number = 800;
  public height: number = 800;
  public columns: number = 10;
  public rows: number = 10;
  constructor(options?: CanvasOptions) {
    if (options) Object.assign(this, options);
  }

  get cell_width() {
    return this.width / this.columns;
  }

  get cell_height() {
    return this.height / this.rows;
  }

  get cell_padding() {
    return this.width / this.columns / 5;
  }
}

export interface I_CanvasProps {
  options: CanvasOptions;
}

export const Canvas = ({ options }: I_CanvasProps) => {
  const canvasRef = useCanvas(options);
  return <canvas role="img" aria-label="A plateu to represent mars for deploying robots" className="border border-black" ref={canvasRef} />;
};

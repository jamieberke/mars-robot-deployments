import { CanvasOptions } from "../shared/canvas/canvas";
import { I_Robot } from "./robot";

export const getRobotOffset = (
  robot: I_Robot,
  canvas_options: CanvasOptions
): Pick<I_Robot, "left_offset" | "top_offset"> => {
  const left_offset =
    robot.starting_x_position * canvas_options.cell_width + robot.width / 2;
  const top_offset =
    robot.starting_y_position * canvas_options.cell_height + robot.height / 2;
  return {
    left_offset,
    top_offset,
  };
};
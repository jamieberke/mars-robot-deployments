import { CanvasOptions } from "../shared/canvas/canvas";
import { getRobotOffset } from "./helper";
import { I_Robot } from "./robot";

export enum E_Robot_Directions {
  NORTH = "N",
  EAST = "E",
  SOUTH = "S",
  WEST = "W",
}

export enum E_Robot_Movements {
  LEFT = "L",
  RIGHT = "R",
  MOVE = "M",
}

export const animation_duration = 7;

const getKeyframeDirection = (
  movement: E_Robot_Movements,
  direction: E_Robot_Directions
): E_Robot_Directions => {
  let new_direction = direction;
  switch (movement) {
    case E_Robot_Movements.RIGHT:
      switch (new_direction) {
        case E_Robot_Directions.NORTH:
          new_direction = E_Robot_Directions.EAST;
          break;
        case E_Robot_Directions.EAST:
          new_direction = E_Robot_Directions.SOUTH;
          break;
        case E_Robot_Directions.SOUTH:
          new_direction = E_Robot_Directions.WEST;
          break;
        case E_Robot_Directions.WEST:
          new_direction = E_Robot_Directions.NORTH;
          break;
        default:
          break;
      }
      break;
    case E_Robot_Movements.LEFT:
      switch (new_direction) {
        case E_Robot_Directions.NORTH:
          new_direction = E_Robot_Directions.WEST;
          break;
        case E_Robot_Directions.EAST:
          new_direction = E_Robot_Directions.NORTH;
          break;
        case E_Robot_Directions.SOUTH:
          new_direction = E_Robot_Directions.EAST;
          break;
        case E_Robot_Directions.WEST:
          new_direction = E_Robot_Directions.SOUTH;
          break;
        default:
          break;
      }
      break;
  }
  return new_direction;
};

const getKeyframeOffset = (
  movement: E_Robot_Movements,
  direction: E_Robot_Directions,
  top: number,
  left: number,
  canvas_options: CanvasOptions
): Record<string, number> => {
  let new_direction = direction;
  let new_top = top;
  let new_left = left;
  switch (movement) {
    case E_Robot_Movements.MOVE:
      switch (new_direction) {
        case E_Robot_Directions.NORTH:
          new_top = new_top - canvas_options.cell_height;
          break;
        case E_Robot_Directions.EAST:
          new_left = new_left + canvas_options.cell_width;
          break;
        case E_Robot_Directions.SOUTH:
          new_top = new_top + canvas_options.cell_height;
          break;
        case E_Robot_Directions.WEST:
          new_left = new_left - canvas_options.cell_width;
          break;
        default:
          break;
      }
      break;
    default:
      break;
  }

  return {
    new_top,
    new_left,
  };
};

const getKeyframeRotation = (
  direction: E_Robot_Directions,
  movement: E_Robot_Movements
) => {
  switch (direction) {
    case E_Robot_Directions.NORTH:
      switch (movement) {
        case E_Robot_Movements.RIGHT:
          return 90;
        case E_Robot_Movements.LEFT:
          return -90;
        default:
          return 0;
      }
    case E_Robot_Directions.EAST:
      switch (movement) {
        case E_Robot_Movements.RIGHT:
          return 180;
        case E_Robot_Movements.LEFT:
          return 0;
        default:
          return 0;
      }
    case E_Robot_Directions.SOUTH:
      switch (movement) {
        case E_Robot_Movements.RIGHT:
          return 270;
        case E_Robot_Movements.LEFT:
          return -270;
        default:
          return 0;
      }
    case E_Robot_Directions.WEST:
      switch (movement) {
        case E_Robot_Movements.RIGHT:
          return 360;
        case E_Robot_Movements.LEFT:
          return -180;
        default:
          return 0;
      }
    default:
      return 0;
  }
};

export const generateKeyframes = (
  robot: I_Robot,
  animationName: string,
  canvas_options: CanvasOptions
): string => {
  const animation_interval = 100 / robot.movements.length;
  let keyframes_string = `@keyframes ${animationName} {
      0% {
          left: ${robot.left_offset}px;
          top: ${robot.top_offset}px;
          transform ${robot.rotation};
      }`;
  let new_left = robot.left_offset;
  let new_top = robot.top_offset;
  let new_direction = robot.direction;
  let new_rotation = robot.rotation;
  robot.movements.forEach((movement, i) => {
    if (movement !== E_Robot_Movements.MOVE) {
      new_rotation = getKeyframeRotation(new_direction, movement);
    }
    new_direction = getKeyframeDirection(movement, new_direction);
    const new_offsets = getKeyframeOffset(
      movement,
      new_direction,
      new_top,
      new_left,
      canvas_options
    );
    new_top = new_offsets.new_top;
    new_left = new_offsets.new_left;
    keyframes_string += `${animation_interval * (i + 1)}% {
      top: ${new_top}px; left:${new_left}px; transform: rotate(${new_rotation}deg);
    }`;
  });
  return keyframes_string;
};

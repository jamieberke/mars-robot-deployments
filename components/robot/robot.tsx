import { CanvasOptions } from "../shared/canvas/canvas";
import {
  E_Robot_Directions,
  E_Robot_Movements,
  generateKeyframes,
  animation_duration,
} from "./animations";
import { getRobotOffset } from "./helper";

export interface I_Robot {
  starting_x_position: number;
  starting_y_position: number;
  id: number;
  direction: E_Robot_Directions;
  movements: Array<E_Robot_Movements>;
  height: number;
  width: number;
  top_offset: number;
  left_offset: number;
  rotation: number;
}

interface RobotProps {
  robot: I_Robot;
  canvas_options: CanvasOptions;
}

const getDirectionalClasses = (direction: E_Robot_Directions): string => {
  switch (direction) {
    case E_Robot_Directions.EAST:
      return "rotate-90";
    case E_Robot_Directions.SOUTH:
      return "rotate-180";
    case E_Robot_Directions.WEST:
      return "rotate-270;";
    default:
      return "";
  }
};

const Robot = ({ robot, canvas_options }: RobotProps) => {
  robot.width = canvas_options.cell_width / 2;
  robot.height = canvas_options.cell_height / 2;
  const robot_offsets = getRobotOffset(robot, canvas_options);
  robot.top_offset = robot_offsets.top_offset;
  robot.left_offset = robot_offsets.left_offset;
  switch (robot.direction) {
    case E_Robot_Directions.NORTH:
      robot.rotation = 0;
      break;
    case E_Robot_Directions.EAST:
      robot.rotation = 90;
      break;
    case E_Robot_Directions.SOUTH:
      robot.rotation = 180;
      break;
    case E_Robot_Directions.WEST:
      robot.rotation = 270;
      break;
    default:
      break;
  }
  const styleSheet = document.styleSheets[0];
  const animationName = `robot_animation${robot.id}`;
  const directionClasses = getDirectionalClasses(robot.direction);

  styleSheet.insertRule(
    generateKeyframes(robot, animationName, canvas_options),
    styleSheet.cssRules.length
  );

  const robot_animation = {
    animationName,
    animationTimingFunction: "ease-in-out",
    animationDuration: `${animation_duration}s`,
    animationDirection: "normal",
    animationFillMode: "forwards",
    animationDelay: `${(robot.id - 1) * animation_duration}s`,
  };
  const robot_directional_border_width: number = canvas_options.cell_width / 8;
  return (
    <div
      className={`bg-red-900 rounded-full flex items-center justify-center ${directionClasses}`}
      style={{
        ...robot_animation,
        position: "absolute",
        top: robot.top_offset,
        left: robot.left_offset,
        width: robot.width,
        height: robot.height,
      }}
    >
      <span
        style={{
          borderLeftWidth: `${robot_directional_border_width}px`,
          borderRightWidth: `${robot_directional_border_width}px`,
          borderBottomWidth: `${robot_directional_border_width}px`,
        }}
        className={`border-red-900 absolute border-x-transparent bottom-full -mb-0.5`}
      ></span>
    </div>
  );
};

export default Robot;
